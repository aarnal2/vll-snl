### Hello there! ###
This repository is used to store work done in the Vision & Learning Lab and Systems Neuroscience Lab at the University of Texas at El Paso.
### Contents ###
* PDFs and figures relevant to problem
* Code to preprocess data, build models, train, and compute  
* Some folders may contain make files (Specified in individual "README.md" file)
### Contact ###
* Phone: 915-588-1690
* Email: alex51195@gmail.com
* Email: aarnal2@miners.utep.edu
