#!/bin/sh
modelNum=$1
i=0
maxEpoch=$2

while [ "$i" -le "$maxEpoch" ]; do
   python3 train_uNet.py $modelNum $i
    i=$(( i + 1 ))
done


