#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  6 15:50:29 2019

@author: bigweenie
https://www.kaggle.com/sajaldeb25/2018-data-science-bowl-solved-by-resunet
"""

import os
import sys
import warnings
import numpy as np
import cv2
import tensorflow as tf
import pandas
from skimage.io import imread, imsave
from keras.models import Model, load_model
from keras.layers import Input
from keras.layers.core import Dropout, Lambda
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate
from keras import backend as K

#Uncomment if you need to specify GPU card
#os.environ['CUDA_DEVICE_ORDER']="PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"]="1"

def mean_iou(y_true, y_pred):
    prec = []
    for t in np.arange(0.5, 1.0, 0.05):
        y_pred_ = tf.to_int32(y_pred > t)
        score, up_opt = tf.metrics.mean_iou(y_true, y_pred_, 2)
        K.get_session().run(tf.local_variables_initializer())
        with tf.control_dependencies([up_opt]):
            score = tf.identity(score)
        prec.append(score)
    return K.mean(K.stack(prec), axis=0)

def buildUNet():
    inputs = Input((IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS))
    s = Lambda(lambda x: x / 255) (inputs)
    
    c1 = Conv2D(16, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (s)
    c1 = Dropout(0.1) (c1)
    c1 = Conv2D(16, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (c1)
    p1 = MaxPooling2D((2, 2)) (c1)
    
    c2 = Conv2D(32, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (p1)
    c2 = Dropout(0.1) (c2)
    c2 = Conv2D(32, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (c2)
    p2 = MaxPooling2D((2, 2)) (c2)
    
    c3 = Conv2D(64, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (p2)
    c3 = Dropout(0.2) (c3)
    c3 = Conv2D(64, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (c3)
    p3 = MaxPooling2D((2, 2)) (c3)
    
    c4 = Conv2D(128, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (p3)
    c4 = Dropout(0.2) (c4)
    c4 = Conv2D(128, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (c4)
    p4 = MaxPooling2D(pool_size=(2, 2)) (c4)
    
    c5 = Conv2D(256, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (p4)
    c5 = Dropout(0.3) (c5)
    c5 = Conv2D(256, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (c5)
    
    u6 = Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same') (c5)
    u6 = concatenate([u6, c4])
    c6 = Conv2D(128, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (u6)
    c6 = Dropout(0.2) (c6)
    c6 = Conv2D(128, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (c6)
    
    u7 = Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same') (c6)
    u7 = concatenate([u7, c3])
    c7 = Conv2D(64, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (u7)
    c7 = Dropout(0.2) (c7)
    c7 = Conv2D(64, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (c7)
    
    u8 = Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same') (c7)
    u8 = concatenate([u8, c2])
    c8 = Conv2D(32, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (u8)
    c8 = Dropout(0.1) (c8)
    c8 = Conv2D(32, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (c8)
    
    u9 = Conv2DTranspose(16, (2, 2), strides=(2, 2), padding='same') (c8)
    u9 = concatenate([u9, c1], axis=3)
    c9 = Conv2D(16, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (u9)
    c9 = Dropout(0.1) (c9)
    c9 = Conv2D(16, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (c9)
    
    outputs = Conv2D(1, (1, 1), activation='sigmoid') (c9)
    
    return Model(inputs=[inputs], outputs=[outputs])

def loadData():
    lista = os.listdir('cropped/images/output/images/')
    X_train = np.zeros((len(lista), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
    Y_train = np.zeros((len(lista), IMG_HEIGHT, IMG_WIDTH, 1), dtype=np.bool)
    sys.stdout.flush()
    
    for n,fname in enumerate(lista):
        id_ = fname[16:]
        im = imread('cropped/images/output/images/images_original_%s'%id_)
        X_train[n] = im
        mask = np.sum(imread('cropped/images/output/mask/_groundtruth_(1)_images_%s'%id_),axis=-1)< 100
        Y_train[n] = np.expand_dims(mask, axis=-1) 
        
    X_test = np.zeros((2, IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
    im = cv2.resize(imread('cropped/lvl31im.png'), (IMG_HEIGHT,IMG_WIDTH))
    X_test[0] = im
    im = cv2.resize(imread('cropped/images/lvl21_1.png'), (IMG_HEIGHT,IMG_WIDTH))
    X_test[1] = im
    return X_train, Y_train, X_test

warnings.filterwarnings('ignore', category=UserWarning, module='skimage')

IMG_WIDTH = 512
IMG_HEIGHT = 512
IMG_CHANNELS = 3
modelNum = int(sys.argv[1])
currentEpoch = int(sys.argv[2])

print('\n\n\t\t\t|   |  | | ||Current Epoch %d|| | |  |  |\n'%currentEpoch)

try:
    model = load_model('models/uNet%d.h5'%modelNum, 
                       custom_objects={'mean_iou': mean_iou})
    print('Model models/uNet%d.h5 loaded...'%modelNum)
    
except:
    print('Building model...')
    model = buildUNet()
    model.compile(optimizer='adam',loss='binary_crossentropy', 
                  metrics=[mean_iou])
    file = open('%d_history.csv'%modelNum,'a')
    file.write('epoch,loss,mean_iou,val_loss,val_mean_iou')
    file.close()
    
#insert code to generate data with augmentor
X_train, Y_train, X_test = loadData()

# Fit model
print('Fitting...')
results = model.fit(X_train, Y_train, validation_split=0.2, 
                    batch_size=20, epochs=1)
model.save('models/uNet%d.h5'%modelNum)

# Conditionally save best model
if currentEpoch > 0:
    colnames = ['epoch','loss','mean_iou','val_loss','val_mean_iou']
    data = pandas.read_csv('%d_history.csv'%modelNum, names=colnames)
    val_losses = data.val_loss.tolist()
    min_val_loss = min(np.asarray(val_losses[1:]).astype(float))
    val_loss = results.history['val_loss'][0]
    if val_loss < min_val_loss:
        print('val_loss:\t\t%f\nmin(val_losses):\t%f'%(val_loss,min_val_loss))
        print('Saving Model')
        model.save('models/uNet%dBest.h5'%modelNum)
    
        # Predict on train, val and test
        preds_train = model.predict(X_train[0:2], verbose=1)
        preds_val = model.predict(X_train[0:2], verbose=1)
        preds_test = model.predict(X_test, verbose=1)
            
        # Threshold predictions
        thr = 0.5
        preds_train_t = (preds_train > thr).astype(np.uint8)
        preds_val_t = (preds_val > thr).astype(np.uint8)
        preds_test_t = (preds_test > thr).astype(np.uint8)
    
        imsave('%d_%dpredTrain.png'%(modelNum,currentEpoch),preds_train_t[0]*255)
        imsave('%d_%dpredVal.png'%(modelNum,currentEpoch),preds_val_t[0]*255)
        imsave('%d_%dpredLvl31im.png'%(modelNum,currentEpoch),preds_test_t[0]*255)
        imsave('%d_%dpredLvl21_1.png'%(modelNum,currentEpoch),preds_test_t[1]*255)
        

file = open('%d_history.csv'%modelNum,'a')
file.write('\n%d,%f,%f,%f,%f'%(currentEpoch,
                             results.history['loss'][0],
                             results.history['mean_iou'][0],
                             results.history['val_loss'][0],
                             results.history['val_mean_iou'][0]))
file.close()
