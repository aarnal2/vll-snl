#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 15 17:58:33 2019

Sample anchor, neighbor, and distant tile
Play with tile size, are we resampling?

@author: bigweenie
"""
import random
import numpy as np
import matplotlib.pyplot as plt
from skimage.external.tifffile import imread, imshow


#                           Function Definitions
def neighbor(r,a_row,a_col):
    a = random.random() * 2 * np.pi
    row = (random.randint(1,r) * np.cos(a) + a_row).astype(int)
    col = (random.randint(1,r) * np.sin(a) + a_col).astype(int)
    return row, col

def distant(r,max_distance,a_row,a_col):
    a = random.random() * 2 * np.pi
    row = (random.randint(r,max_distance) * np.cos(a) + a_row).astype(int)
    col = (random.randint(r,max_distance) * np.sin(a) + a_col).astype(int)
    return row, col

def sample(D,kind,frame_size,a_row,a_col,r,max_distance):
    if kind=='anchor':
        row = random.randint(0, D.shape[0]-frame_size)
        col = random.randint(0, D.shape[1]-frame_size)
    elif kind=='neighbor':
        row, col = neighbor(r,a_row,a_col)
    elif kind=='distant':
        row, col = distant(r,max_distance,a_row,a_col)
        
    try:
#        print(row, col)
        frame = D[row:row+frame_size,col:col+frame_size]
    except:
#        print('resample', kind)#uncomment to print when program is in exception
        frame,row,col = sample(D,kind,frame_size,a_row,a_col,r,max_distance)
        
#    print(frame.shape[0], frame.shape[1],'\n')#uncomment to check frames are correct size
    if frame.shape[0]<frame_size or frame.shape[1]<frame_size or row<0 or col<0:
        frame,row,col = sample(D,kind,frame_size,a_row,a_col,r,max_distance)
        
    return frame, row, col

def drawBox(p, color, marker):
    pa = np.array([[p[0][0],p[1][1]],[p[1][0],p[0][1]]])
    plt.plot(np.vstack((p[0, 0], pa[0, 0])), np.vstack((p[0, 1], pa[0, 1])), markersize=1, color=color)#left wall
    plt.plot(np.vstack((p[0, 0], pa[1, 0])), np.vstack((p[0, 1], pa[1, 1])), markersize=1, color=color)#upper wall
    plt.plot(np.vstack((p[1, 0], pa[1, 0])), np.vstack((p[1, 1], pa[1, 1])), markersize=1, color=color)#right wall
    plt.plot(np.vstack((p[1, 0], pa[0, 0])), np.vstack((p[1, 1], pa[0, 1])), markersize=1, color=color)#botm wall
    plt.plot(p[0][0]+20, p[0][1]+20, marker=marker, markersize=5, color=color)
    plt.draw()
    plt.pause(0.01)
    
def showSampledFrame(D,C,s):
    air = 500
    top_r = np.min((C[0][0],C[1][0],C[2][0]))
    low_r = np.max((C[0][0],C[1][0],C[2][0]))
    top_c = np.min((C[0][1],C[1][1],C[2][1]))
    low_c = np.max((C[0][1],C[1][1],C[2][1]))
#    print(top_r,low_r,top_c,low_c)
    top_r = np.min((top_r,np.max((0,top_r-air))))
    low_r = np.max((low_r,np.min((D.shape[0]-1,low_r+air+s))))
    top_c = np.min((top_c,np.max((0,top_c-air))))
    low_c = np.max((low_c,np.min((D.shape[1]-1,low_c+air+s))))
#    print(top_r,low_r,top_c,low_c)
    plt.imshow(D[top_r:low_r,top_c:low_c])

#                               Main
#Initialize varaibles
D = imread('../images/18 016 1 LHA s3 7\'3\'18/TH15 s3t5_102.tif', key=0)
N = 10 #number of tile triplets
s = 128 #tile size
r = 100 #neighborhood radius
c = D.shape[2] #number of channels
max_distance = np.min((D.shape[0],D.shape[1]))#boundary of distant tile
T = np.zeros((N, 3, s, s, c), dtype=np.uint8)#initialize triplet list
C = np.zeros((N, 3, 2), dtype=np.int)#initialize triplet coordintes list
for i in range(0,N):
    T[i][0], a_row, a_col = sample(D,'anchor',s,0,0,0,0)
    C[i][0][0] = a_row
    C[i][0][1] = a_col
    T[i][1], C[i][1][0], C[i][1][1] = sample(D,'neighbor',s,a_row,a_col,r,0)
    T[i][2], C[i][2][0], C[i][2][1] = sample(D,'distant',s,a_row,a_col,r,max_distance-1)
    
#check tiled samples
ix = random.randint(0,N-1)#
imshow(T[ix][0])#anchor
imshow(T[ix][1])#neighbor
imshow(T[ix][2])#distant
showSampledFrame(D,C[ix],s)#show
color = ['green','blue','red']#set colors for anchor boxes
imshow(D)#set up canvas to draw boxes
for i in range(0,C.shape[1]):#draw boxes
    p = np.array([[C[ix][i][1],C[ix][i][0]],[C[ix][i][1]+s,C[ix][i][0]+s]])
    drawBox(p,color[i],'$%s$'%str(ix))
